package ru.intervi.fixwhitelist;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.google.gson.Gson;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.File;
import java.util.UUID;

public class Main extends JavaPlugin implements Listener {
	private final String PATH = getPath() + File.separator + "whitelist.json";
	private final Charset ENCODING = Charset.defaultCharset();
	
	private String getPath() {
		String path = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
		path = path.substring(0, path.lastIndexOf(File.separator));
		return path.substring(0, path.lastIndexOf(File.separator));
	}
	
	public class Data {
		public String name;
		public UUID uuid;
	}
	
	@Override
	public void onEnable() {
		this.getServer().getPluginManager().registerEvents(this, this);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPreLogin(PlayerLoginEvent event) {
		if (Bukkit.getOnlineMode()) return;
		if (!event.getResult().equals(Result.KICK_WHITELIST)) return;
		String name = event.getPlayer().getName();
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(PATH));
			String json = new String(encoded, ENCODING);
			Gson gson = new Gson();  
			for (Data d : gson.fromJson(json, Data[].class)) {
				if (d.name.equalsIgnoreCase(name)) {
					event.allow();
					this.getLogger().info(name + " in whitelist, allow");
					break;
				}
			}
		} catch(Exception e) {e.printStackTrace();}
	}
}
